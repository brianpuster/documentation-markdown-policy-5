title: Disaster Recovery Plan Pipeline
author: VerSprite vCISO
published: 2020-06-09

#### Policy name:
Disaster Recovery Plan Pipeline

#### Policy level:
Annual General

#### Author:
VerSprite vCISO

#### Approver(s)
CTO (Bob Ven)  
EVP Finance (Buck Brody)

#### Location or Team Applicability
All Locations, IT Team

#### Effective date:
input date using YYYY-MM-DD format

#### Policy text:

| Role | Name | Phone | Email |
| --- | --- | --- | --- |
| Primary Plan Contact | Joseph Troutman | 770-417-4206 (Office) <br/> 805-550-7564 (mobile) | joseph.troutman@constructconnect.com |
| Alternate Plan Contact | John Stanley | | john.stanley@constructconnect.com |
| Internal SAS Point of Contact | Ken Bradford | | ken.bradford@constructconnect.com |

##### GENERAL

###### Purpose
The purpose of this plan is to ensure a timely recovery of Pipeline platform in an event of a service disruption.


###### Scope
The scope of this plan outlines Pipeline's recovery teams, dependencies, and specific steps required to validate the Pipeline platform.

###### Assumptions
- Technologies in the Lebanon Data Center recover in the West 7th Data Center.
- No two data centers are impacted at the same time.
- Off-site storage locations for backup files and information remain intact and recovery operations are performed in accordance with the priorities set forth by the Business Impact Analysis (BIA) and the procedures/tasks identified in the DR plans.
- Suppliers and their services are available and perform in accordance with contracts and service level agreements (SLAs).
- Recovery tasks/steps are to be validated that ran correctly without any issues. If issues are encountered, they are identified in the DR Capabilities Issues section of this plan.
- Archive & Repo File Servers
    - Built with "no shared storage" replication failover file shares.
    - *File shares automatically fail over to opposing data center in the event of server reboot or failure after no longer than 15 minutes of the primary host being offline.
- "Crucher" Workers only interact with their direct datacenter shares (ex. LEBPRPIPCVW001 only access LEBPRPIPRWR01 share)
 - *In the event of a failover, the failed datacenter role will failover to the active role and retain all of the progress that was made up until the failure.
 - *An automated process will look to see if any activity is left incomplete on failed over node and inject incomplete work into active nodes workers.
- ABBY OCR System:
    - Master Server (GBLPRABBYWR01) - Service is part of Failover cluster which is built into Pipeline Archive Failover Cluster and will failover automatically to the active data center
    - Slave Servers - Processing servers act as independent servers and the master detects if nodes are active and redirect work between active nodes automatically.
- WebServices -Independent web services behind NetScaler load balancer
- SAS Services -Single license service -Only one service can be active at a time!
- In case the primary contact is not available, then the two secondarycontacts must be contacted, since one has the know-how for the processes and the other the permissions to execute such processes
- The Recovery Tasks are to be validated rather than run. If any issues are encountered, should be resolved and documented

###### Objectives
The objectives of this plan are to identify:

- Recovery strategies and steps
- Support teams to becontacted in a recovery situation
- Testing of application; and
- Gaps in DR capability.

##### APPLICATION SYSTEM INFORMATION

| Application Name | Pipeline |
| --- | ---| 
| RTO | 48 hours |
| RPO | 30 seconds |
| Status | Operational |
| Prod Type | Business Application |
| Execution | On-Prem |
| DR Environment | Active/Active on West 7th Data Center |
| Code Location | Primary in Lebanon Data Center and secondary West 7th Data Center |
| Product Owner | Joseph Troutman |
| Description | Backoffice supply product. Takes in raw documents from external sources. Pipeline processes these documents and prepares them to be ingested into ConstructConnect's search engines and systems. |


###### Application Architecture
See [Insert reference to image object]

###### Recovery Team

In an event of a disruption or DR Testing, the recovery team below will be responsible for recovering the Pipeline system. An alternate team lead will be determined by the team leader before this plan is considered complete.


| Recovery Role | Name | Phone | Email |
| --- | --- | --- | --- |
| Recovery Team Lead | Joseph Troutman | 770-417-4206 (office) <br/>805-550-7564 (mobile) | joseph.troutman@constructconnect.com |
| Alternate Recovery Team Lead | John Stanley | | john.stanley@constructconnect.com |
| Recovery Team Member | Vic Myklowycz | | Vic.Myklowycz@constructconnect.com |
| Recovery Team Member | David Muennich | | David.Muennich@constructconnect.com |

###### DR Strategy

Pipeline's current DR strategy is to have the storage in another location, mirroring from the Lebanon data center to the West 7thData center.

In order to failover, we have to go local and break the mirror, repoint from one to the other data center, and bring the services back online.

##### RECOVERY REQUIREMENTS

###### Recovery Tasks

**A) Archive File Services**

1. Login to W7XPRPIPAWP01.prod.buildonetech.co or LEBPRPIPAWP01.prod.buildonetech.co Server
1. Open Failover Cluster Manager.
1. Verify GBLPRPIPAWR01.prod.buildonetech.co (Archive File Share) is up and failed over to active node
1. VerifyGBLPRABBYWR01.prod.buildonetech.co (ABBYY Master Service) is up and failed over to active nodeB )

**B) Repository File Services**

1. Login to W7XPRPIPRWP01.prod.buildonetech.co or LEBPRPIPRWP01.prod.buildonetech.co Server
1. Open Failover Cluster Manager.
1. Verify LEBPRPIPRWR01.prod.buildonetech.co (Lebanon File Share) is up and failed over to active node
1. Verify W7XPRPIPRWR01.prod.buildonetech.co (West7th File Share) is up and failed over to active node

**C) Cruncher Workers**

- Workers only connect to Local datacenter file shares BY DESIGN.
- Offline nodes will remain offline until datacenter returns to Normal Operating Procedure BY DESIGND)

**D) Web Services**

1. Login to Citrix NetScaler
1. Navigate to Traffic Management >> Load Balancing >> Service Groups >> pool_Prod_Pipeline_WebServices
1. Verify that the active datacenter node is functioning

**E) SAS Services -Only ONE active service at any one time due to license restrictions**

1. Power on Active Datacenter node (if powered down)
1. Login to Citrix NetScaler.
1. Navigate to Traffic Management >> Load Balancing >> Service Groups >> pool_Prod_Pipeline_SAS_Services
1. Manually disable the inactive datacenter node (if not active)
1. Contact Product Support Team (Ken Bradford's team as of this writing) for any issues with SAS Application.

**F) ABBYY OCR**

1. Login to either LEBPRPIPAPW01 or W7XPRPIPAPW01 server (which ever one is in operating datacenter)
1. Launch START >> ABBYY FineReader Server >> Remote Administration Console
1. Console will attempt to login to LOCALHOST server -cancel it.
1. Using MMC plugin tree on left hand side of window, find "ABBYY FineReader Servers" (located in Upper left corner), Right-Click and choose "Register New Server"
1. Server Name = GBLPRABBYWR01 / Location = GBLPRABBYWR01.prod.buildonetech.co/ Authentication = Login as "Chief Administrator" / Password = (located in LastPass) / Click TEST Connection >> Click OK
1. Open Stations >> Processing Stations; make sure that active datacenter Group is ENABLED. DISABLE deactivated datacenter to prevent ABBYY from wasting cycles trying to active dead machines.
1. Open Workflows and verify that all PROD workloads are currently running (Restart Workflows if needed). **Note:** ALL Workflows need to be running! -All Datacenter Crunchers automatically fetch files from same paths.

**G) VDI / Citrix**

1. VDI environment is deployed in Google Cloud Platform with redundant Google Direct Connections in each datacenter.
1. Contact VDI Admin (Matt Lehrian as of this update) for current status / issues.

**H) Database Services**

1. All Pipeline services utilize AlwaysOn SQL Connection groups with automated failover.
1. Database requirements are managed and maintained by JAMS JOBS; no SQL connections utilized for any other part of Pipeline Application*
1. Check with DBA services to make sure all Pipeline Database Servers are up and running after failover. **Note:** SAS application utilized a local Database connection on its own servers; completely independent of Pipeline -SAS is to be treated like an isolated appliance.

**I) BidCenter (aka EPOP)**

1. BidCenter utilizes a dedicated subset of Pipeline "Crunchers" (specifically W7XPRPIPCVW04.prod.buildonetech.co& LEBPRPIPCVW04.prod.buildonetech.co) for image processing.
1. Verify with JAMS Administrator / Operations Manager that BidCenter processes successfully moved or are still running on Active Datacenter copy of above listed cruncher.
    1. Restart JAMS Jobs on JAMS Master if issues occur.
    1. Restart JAMS Agent on Active Datacenter EPOP "Cruncher" if required.

###### Internal Application Dependencies


| Dependency Type | Key/Name |
| --- | --- | 
| Sends documents to Insight repo and AWS Bucket. Insight then indexes that information using Apache Solr | Insight |
| Needs James execute function jobs/tasks | Jams |

###### Third Party Service Dependencies

| Third Party | Service | Service Description |
| --- | --- | --- |
| S3 (AWS) | Cloud | Storage location for end results from Pipeline |
| SAS Product | | Single license service |
| Abbyy | OCR Processing Service | Take .pdf and .tiff and translate them into text. |
| Microsoft Storage Replica Services | Storage Replica | Real-time replication factoring into servers (version of windows server) |

###### Identified DR Capability Issues

No network capacity to run a synchronous replication. Currently running asynchronous and expecting network update to resolve this gap Q2 2020.

###### Testing History

| Date | Test Scenario | Result (Pass/Fail) | Result Comments |
| ---- | ------------- | ------------------ | --------------- |
| | | | |
| | | | |
| | | | |


##### REFERENCES

###### Relevant Regulations.

- PCI-DSS v3.2.1

###### Relevant NIST CSF Domain(s)/Category(ies)/Subcategory(ies).

- Identify
- Respond
- Recover

###### Relevant Roper CIS Domain(s)/Category(ies)/Subcategory(ies).

- 1.1, 1.2, 1.3, 10.1, 10.2, 14.1, 16.1, 16.2

###### Related Policies, Plans, Procedures and Guidelines.

- Written Information Security Program
- BCDR Policy
- Emergency Procedure Manual
- DR Runbook

#### Insert any procedure or process documentation (optional):
